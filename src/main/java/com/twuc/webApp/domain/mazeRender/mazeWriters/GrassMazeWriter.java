package com.twuc.webApp.domain.mazeRender.mazeWriters;

import org.springframework.stereotype.Component;

@Component
public class GrassMazeWriter extends NormalMazeWriter {
    public GrassMazeWriter(GrassMazeComponentFactory factory) {
        super(factory);
    }

    @Override
    public String getName() {
        return "grass";
    }
}

